//
// Created by legokol on 13.03.23.
//

#include <gtest/gtest.h>

#include "computational-math/interpolation/Lagrange.hpp"
#include "computational-math/interpolation/Newton.hpp"

TEST(Interpolation, Lagrange) {
    const std::vector<double> x0{0, 1, 2, 3, 4, 5, 6, 7};
    const std::vector<double> f0{0, 3, 2, 6, 4, 8, 1, 5};

    const computational_math::interpolation::LagrangeInterpolator interpolator(x0, f0);

    for (std::size_t i = 0; i < x0.size(); ++i) {
        EXPECT_DOUBLE_EQ(f0[i], interpolator(x0[i]));
    }
}

TEST(Interpolation, Newton) {
    const std::vector<double> x0{0, 1, 2, 3, 4, 5, 6, 7};
    const std::vector<double> f0{0, 3, 2, 6, 4, 8, 1, 5};

    const computational_math::interpolation::NewtonInterpolator interpolator(x0, f0);

    for (std::size_t i = 0; i < x0.size(); ++i) {
        EXPECT_NEAR(f0[i], interpolator(x0[i]), 3e-13);
    }
}

TEST(Interpolation, Compare) {
    const std::vector<double> x0{0, 1, 2, 3, 4, 5, 6, 7};
    const std::vector<double> f0{0, 3, 2, 6, 4, 8, 1, 5};

    const computational_math::interpolation::LagrangeInterpolator lagrange(x0, f0);
    const computational_math::interpolation::NewtonInterpolator newton(x0, f0);

    for (double x = 0; x < 7; x += 0.1) {
        EXPECT_NEAR(newton(x), lagrange(x), 3e-13);
    }
}
