//
// Created by legokol on 13.03.23.
//

#ifndef COMPUTATIONALMATH_LAGRANGE_HPP
#define COMPUTATIONALMATH_LAGRANGE_HPP

#include <vector>

namespace computational_math::interpolation {

template <typename ArgType, typename ValueType>
class LagrangeInterpolator {
    std::vector<ArgType> x_;
    std::vector<ValueType> coefficients_;

public:
    LagrangeInterpolator(const std::vector<ArgType> &x, const std::vector<ValueType> &f);

    [[nodiscard]] ValueType operator()(ArgType x) const;
};

template <typename ArgType, typename ValueType>
LagrangeInterpolator<ArgType, ValueType>::LagrangeInterpolator(const std::vector<ArgType> &x,
                                                               const std::vector<ValueType> &f)
    : x_(x), coefficients_(f) {
    for (std::size_t i = 0; i < x.size(); ++i) {
        for (std::size_t j = 0; j < x.size(); ++j) {
            if (i != j) {
                coefficients_[i] /= (x[i] - x[j]);
            }
        }
    }
}

template <typename ArgType, typename ValueType>
ValueType LagrangeInterpolator<ArgType, ValueType>::operator()(ArgType x) const {
    ValueType res = coefficients_[0];
    for (std::size_t j = 1; j < x_.size(); ++j) {
        res *= (x - x_[j]);
    }
    for (std::size_t i = 0; i < x_.size(); ++i) {
        ValueType product = coefficients_[i];
        for (std::size_t j = 0; j < i; ++j) {
            product *= (x - x_[j]);
        }
        for (std::size_t j = i + 1; j < x_.size(); ++j) {
            product *= (x - x_[j]);
        }
        res += product;
    }
    return res;
}

}  // namespace ComputationalMath::Interpolation

#endif  // COMPUTATIONALMATH_LAGRANGE_HPP
