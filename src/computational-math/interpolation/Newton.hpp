//
// Created by legokol on 13.03.23.
//

#ifndef COMPUTATIONALMATH_NEWTON_HPP
#define COMPUTATIONALMATH_NEWTON_HPP

#include <numeric>
#include <vector>

namespace computational_math::interpolation {

template <typename ArgType, typename ValueType>
class NewtonInterpolator {
    std::vector<ArgType> x_;
    std::vector<ValueType> coefficients_;

public:
    NewtonInterpolator(const std::vector<ArgType> &x, const std::vector<ValueType> &f);

    [[nodiscard]] ValueType operator()(ArgType x) const;
};

template <typename ArgType, typename ValueType>
NewtonInterpolator<ArgType, ValueType>::NewtonInterpolator(const std::vector<ArgType> &x,
                                                           const std::vector<ValueType> &f)
    : x_{x}, coefficients_(x.size()) {
    coefficients_[0] = f[0];
    std::vector<ValueType> dividedDifference{f};
    for (std::size_t i = 1; i < x.size(); ++i) {
        for (std::size_t j = 0; j < x.size() - i; ++j) {
            dividedDifference[j] = (dividedDifference[j + 1] - dividedDifference[j]) / (x[j + i] - x[j]);
        }
        coefficients_[i] = dividedDifference[0];
    }
}

template <typename ArgType, typename ValueType>
ValueType NewtonInterpolator<ArgType, ValueType>::operator()(ArgType x) const {
    ValueType res = coefficients_[0];
    for (std::size_t i = 1; i < x_.size(); ++i) {
        const ValueType product =
            std::accumulate(x_.begin(), x_.begin() + static_cast<std::ptrdiff_t>(i), coefficients_[i],
                            [x](ValueType left, ValueType right) { return left * (x - right); });
        res += product;
    }
    return res;
}

}  // namespace ComputationalMath::Interpolation

#endif  // COMPUTATIONALMATH_NEWTON_HPP
