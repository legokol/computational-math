//
// Created by legokol on 12.03.23.
//

#ifndef COMPUTATIONAL_MATH_CUBICSPLINE_HPP
#define COMPUTATIONAL_MATH_CUBICSPLINE_HPP

#include <algorithm>
#include <optional>
#include <ranges>
#include <vector>

#include "computational-math/utils/SolveTridiagonalSLE.hpp"

namespace computational_math::interpolation {

template <typename ArgType, typename ValueType>
class CubicSpline {
    struct Coefficients {
        ValueType a, b, c, d;
    };

    std::vector<ArgType> x_;                  // Шаблон интерполяции
    std::vector<Coefficients> coefficients_;  // Коэффициенты полиномов

    std::optional<std::size_t> find(ArgType x) const noexcept;

public:
    CubicSpline(const std::vector<ArgType> &x, const std::vector<ValueType> &f);

    [[nodiscard]] std::optional<ValueType> interpolate(ArgType x) const noexcept;
    [[nodiscard]] std::optional<ValueType> operator()(ArgType x) const noexcept { return interpolate(x); }

    [[nodiscard]] std::optional<ValueType> interpolateDerivative(ArgType x, std::size_t order) const noexcept;
};

template <typename ArgType, typename ValueType>
CubicSpline<ArgType, ValueType>::CubicSpline(const std::vector<ArgType> &x, const std::vector<ValueType> &f)
    : x_{x}, coefficients_(x.size() - 1) {
    if (x.size() == 2) {
        const ValueType zero = f[0] - f[0];
        coefficients_[0].a   = f[1];
        coefficients_[0].b   = (f[1] - f[0]) / (x[1] - x[0]);
        coefficients_[0].c   = zero;
        coefficients_[0].d   = zero;
        return;
    }
    if (x.size() == 3) {
        const ArgType step1 = x[1] - x[0];
        const ArgType step2 = x[2] - x[1];
        const ArgType step  = x[2] - x[0];

        const ValueType devDiv2 = (f[2] - f[1]) / step2;
        const ValueType devDiv1 = (f[1] - f[0]) / step1;
        const ValueType zero    = f[0] - f[0];

        coefficients_[0].a = f[1];
        coefficients_[1].a = f[2];

        coefficients_[0].c = static_cast<ArgType>(3) * (devDiv2 - devDiv1) / step;
        coefficients_[1].c = zero;

        coefficients_[0].d = coefficients_[0].c / step1 / static_cast<ArgType>(6);
        coefficients_[1].d = -coefficients_[0].c / step2 / static_cast<ArgType>(6);

        coefficients_[0].b = coefficients_[0].c * step1 / static_cast<ArgType>(3) + devDiv1;
        coefficients_[1].b = coefficients_[0].b + coefficients_[0].c * step2 / static_cast<ArgType>(2);

        coefficients_[0].c /= 2;
        return;
    }

    std::vector<ArgType> a(x.size() - 2);
    std::vector<ArgType> b(x.size() - 2, 2);
    std::vector<ArgType> c(x.size() - 2);
    std::vector<ValueType> d(x.size() - 2);

    {
        ArgType h0;
        ArgType h1 = x[1] - x[0];
        ValueType df0;
        ValueType df1 = (f[1] - f[0]) / h1;
        for (std::size_t i = 0; i < d.size(); ++i) {
            h0              = std::exchange(h1, x[i + 2] - x[i + 1]);
            df0             = std::exchange(df1, (f[i + 2] - f[i + 1]) / h1);
            const ArgType h = x[i + 2] - x[i];

            a[i] = h0 / h;
            c[i] = h1 / h;
            d[i] = static_cast<ArgType>(6) * (df1 - df0) / h;
        }
    }
    const auto solution = utils::solveTridiagonalSLE(a, b, c, d);

    std::ranges::copy(solution, (coefficients_ | std::views::transform(&Coefficients::c)).begin());
    coefficients_.back().c = 0;

    {
        const ArgType h    = x[1] - x[0];
        coefficients_[0].a = f[1];
        coefficients_[0].b = coefficients_[0].c * h / static_cast<ArgType>(3) + (f[1] - f[0]) / h;
        coefficients_[0].d = coefficients_[0].c / h;
    }

    for (std::size_t i = 1; i < coefficients_.size(); ++i) {
        const ArgType h    = x[i + 1] - x[i];
        coefficients_[i].a = f[i + 1];
        coefficients_[i].b =
            coefficients_[i - 1].b + (coefficients_[i].c + coefficients_[i - 1].c) * h / static_cast<ArgType>(2);
        coefficients_[i].d = (coefficients_[i].c - coefficients_[i - 1].c) / h;
    }

    for (auto &elem : coefficients_) {
        elem.c /= 2;
        elem.d /= 6;
    }
}

template <typename ArgType, typename ValueType>
std::optional<std::size_t> CubicSpline<ArgType, ValueType>::find(ArgType x) const noexcept {
    if (x < x_.front() || x > x_.back()) {
        return std::nullopt;
    }
    const auto it = std::lower_bound(x_.begin(), x_.end(), x);
    if (it == x_.begin()) {
        return 0;
    } else {
        return static_cast<std::size_t>(it - x_.begin() - 1);
    }
}

template <typename ArgType, typename ValueType>
std::optional<ValueType> CubicSpline<ArgType, ValueType>::interpolate(ArgType x) const noexcept {
    const std::optional<std::size_t> opt = find(x);
    if (!opt) {
        return std::nullopt;
    }
    const std::size_t i = *opt;
    const ArgType d     = x - x_[i + 1];
    return (coefficients_[i].a + coefficients_[i].b * d + coefficients_[i].c * d * d + coefficients_[i].d * d * d * d);
}

template <typename ArgType, typename ValueType>
std::optional<ValueType> CubicSpline<ArgType, ValueType>::interpolateDerivative(ArgType x,
                                                                                std::size_t order) const noexcept {
    const std::optional<std::size_t> opt = find(x);
    if (!opt) {
        return std::nullopt;
    }
    const std::size_t i = *opt;
    const ArgType d     = x - x_[i + 1];

    if (order == 0) {
        return (coefficients_[i].a + coefficients_[i].b * d + coefficients_[i].c * d * d +
                coefficients_[i].d * d * d * d);
    } else if (order == 1) {
        return (coefficients_[i].b + static_cast<ArgType>(2) * coefficients_[i].c * d +
                static_cast<ArgType>(3) * coefficients_[i].d * d * d);
    } else if (order == 2) {
        return (static_cast<ArgType>(2) * coefficients_[i].c + static_cast<ArgType>(6) * coefficients_[i].d * d);
    } else if (order == 3) {
        return (static_cast<ArgType>(6) * coefficients_[i].d);
    } else {
        return coefficients_[0].a * static_cast<ArgType>(0);
    }
}

}  // namespace computational_math::interpolation

#endif  // COMPUTATIONAL_MATH_CUBICSPLINE_HPP
