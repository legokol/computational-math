//
// Created by legokol on 13.03.23.
//

#ifndef COMPUTATIONALMATH_SOLVETRIDIAGONALSLE_HPP
#define COMPUTATIONALMATH_SOLVETRIDIAGONALSLE_HPP

#include <vector>

namespace computational_math::utils {

template <typename T1, typename T2>
std::vector<T2> solveTridiagonalSLE(const std::vector<T1> &a, const std::vector<T1> &b, const std::vector<T1> &c,
                                    const std::vector<T2> &d) {
    std::vector<T1> p(d.size() - 1);
    std::vector<T2> q(d.size() - 1);
    std::vector<T2> x(d.size());

    p[0] = -c[0] / b[0];
    q[0] = d[0] / b[0];
    for (std::size_t i = 1; i < p.size(); ++i) {
        p[i] = -c[i] / (a[i] * p[i - 1] + b[i]);
        q[i] = (d[i] - a[i] * q[i - 1]) / (a[i] * p[i - 1] + b[i]);
    }
    x.back() = (d.back() - a.back() * q.back()) / (a.back() * p.back() + b.back());

    for (std::size_t j = 2; j <= x.size(); ++j) {
        const std::size_t i = x.size() - j;
        x[i]                = p[i] * x[i + 1] + q[i];
    }

    return x;
}

}  // namespace computational_math::utils

#endif  // COMPUTATIONALMATH_SOLVETRIDIAGONALSLE_HPP
